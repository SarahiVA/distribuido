package puntoTres;

import java.awt.image.BufferedImage;

public class Bloque {
	private BufferedImage img1;
	private BufferedImage img2;
	private BufferedImage imgR;
	private String simbolo;
	private int InicioW;
	private int FinW;
	private int InicioH;
	private int FinH;
	private int alfa;
	private int beta;
	
	public Bloque(String sim, int initW, int endW, int initH, int endH ,BufferedImage A, BufferedImage B)
	{
		this.simbolo = sim;
		this.InicioW = initW;
		this.FinW = endW;
		this.InicioH = initH;
		this.FinH = endH;
		this.img1 = A;
		this.img2 = B;
	}
	
	public BufferedImage getResult(){	return imgR;	} 
	
	public void setAlfa(int nA){	alfa = nA;	}
	
	public void setBeta(int nB){	beta = nB;	}
	
	public void ejecuta()
	{
		switch(simbolo){
		case " + ":
			imgR = ImageOperator.suma(img1, img2, FinW, FinH, InicioW, InicioH);
			break;
		case " - ":
			imgR = ImageOperator.resta(img1, img2, FinW, FinH, InicioW, InicioH);
			break;
		case " * ":
			imgR = ImageOperator.multiplicacion(img1, img2, FinW, FinH, InicioW, InicioH);
			break;
		case "\u03B1 - \u03B2":
			imgR = ImageOperator.combinacionLineal(img1, img2, FinW, FinH, InicioW, InicioH, alfa, beta);
			break;
		}
	}
}


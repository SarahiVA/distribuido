package puntoTres;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.Image;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class ImageOperator {
	
	public int[] factores(int n)
	{
		int divisor = 2;
		ArrayList<Integer> fact = new ArrayList<>();
		while(n != 1)
		{
			if(n % divisor == 0){
				fact.add(divisor);
				n = n / divisor;
			}else{
				divisor++;
			}
		}
		Integer[] aux = new Integer[fact.size()];
		aux = fact.toArray(aux);
		int f1 = 1, f2 = 1;
		for(int i = 0; i < aux.length; i++)
		{
			if(i < aux.length /2){
				f1 = f1 * aux[i];
			}else{
				f2 = f2 * aux[i];
			}
		}
		
		int[] res = {f1,f2};
	return res;
	}
	
	public static ImageIcon redimension(BufferedImage A){
		ImageIcon icono = new ImageIcon(A.getScaledInstance(300, 300, Image.SCALE_SMOOTH));
		return icono;
	}
	public static BufferedImage suma(BufferedImage A, BufferedImage B, int ancho, int alto, int initW, int initH)
	{
		BufferedImage res = new BufferedImage(ancho - initW,alto - initH,BufferedImage.TYPE_INT_RGB);
		Color rgb1, rgb2, auxColor;
		int r, g, b;
		for(int i = initW; i < ancho; i++)
		{
			for(int j = initH; j < alto; j++)
			{
				rgb1 = new Color(A.getRGB(i,j));
				rgb2 = new Color(B.getRGB(i,j));
				if(rgb1.getRed() + rgb2.getRed() <= 255){
					r = rgb1.getRed() + rgb2.getRed();
				}else{
					r = 255;
				}
				if(rgb1.getBlue() + rgb2.getBlue() <= 255){
					g = rgb1.getBlue() + rgb2.getBlue();
				}else{
					g = 255;
				}
				if(rgb1.getGreen() + rgb2.getGreen() <= 255){
					b = rgb1.getGreen() + rgb2.getGreen();
				}else{
					b = 255;
				}
				auxColor = new Color(r,g,b);
				res.setRGB(i - initW, j - initH, auxColor.getRGB());
			}
		}
		
	return res;
	}
	
	public static BufferedImage resta(BufferedImage A, BufferedImage B, int ancho, int alto, int initW, int initH)
	{
		BufferedImage res = new BufferedImage(ancho - initW,alto - initH,BufferedImage.TYPE_INT_RGB);
		Color rgb1, rgb2, auxColor;
		int r, g, b;
		for(int i = initW; i < ancho; i++)
		{
			for(int j = initH; j < alto; j++)
			{
				rgb1 = new Color(A.getRGB(i,j));
				rgb2 = new Color(B.getRGB(i,j));
					r = Math.abs(rgb1.getRed() - rgb2.getRed());
					g = Math.abs(rgb1.getBlue() - rgb2.getBlue());
					b = Math.abs(rgb1.getGreen() - rgb2.getGreen());
				auxColor = new Color(r,g,b);
				res.setRGB(i - initW, j - initH, auxColor.getRGB());
	
			}
		}
		
	return res;
		/*BufferedImage res = new BufferedImage(ancho - initW,alto - initH,BufferedImage.TYPE_INT_RGB);
		Color rgb1, rgb2, auxColor;
		int r, g, b;
		for(int i = initW; i < ancho; i++)
		{
			for(int j = initH; j < alto; j++)
			{
				rgb1 = new Color(A.getRGB(i,j));
				rgb2 = new Color(B.getRGB(i,j));
				if(rgb1.getRed() - rgb2.getRed() >= 0){
					r = rgb1.getRed() - rgb2.getRed();
				}else{
					r = 0;
				}
				if(rgb1.getBlue() - rgb2.getBlue() >= 255){
					g = rgb1.getBlue() - rgb2.getBlue();
				}else{
					g = 0;
				}
				if(rgb1.getGreen() - rgb2.getGreen() >= 255){
					b = rgb1.getGreen() - rgb2.getGreen();
				}else{
					b = 0;
				}
				auxColor = new Color(r,g,b);
				res.setRGB(i - initW, j - initH, auxColor.getRGB());
			}
		}
		
	return res;*/


	}
	
	public static BufferedImage multiplicacion(BufferedImage A, BufferedImage B,int ancho, int alto, int initW, int initH)
	{
		BufferedImage res = new BufferedImage(ancho-initW,alto-initH,BufferedImage.TYPE_INT_RGB);
		Color rgb1, rgb2, auxColor;
		int r, g, b;
		//int k = (1 / 255);
		for(int i = initW; i < ancho; i++)
		{
			for(int j = initH; j < alto; j++)
			{
				rgb1 = new Color(A.getRGB(i,j));
				rgb2 = new Color(B.getRGB(i,j));
					r =  (rgb1.getRed() * rgb2.getRed()) / 255 ;
					g =  (rgb1.getBlue() * rgb2.getBlue()) / 255 ;
					b =  (rgb1.getGreen() * rgb2.getGreen()) / 255;
				auxColor = new Color(r,g,b);
				res.setRGB(i - initW, j - initH, auxColor.getRGB());
			}
		}
		
	return res;
	}
	
	public static BufferedImage combinacionLineal(BufferedImage A, BufferedImage B,
			int ancho, int alto, int initW, int initH, int alfa, int beta)
	{
		BufferedImage res = new BufferedImage(ancho - initW,alto - initH,BufferedImage.TYPE_INT_RGB);
		Color rgb1, rgb2, auxColor;
		int r, g, b;
		double a, be;
		a = (double)alfa / 100;
		be =(double) beta / 100;
		for(int i = initW; i < ancho; i++)
		{
			for(int j = initH; j < alto; j++)
			{
				rgb1 = new Color(A.getRGB(i,j));
				rgb2 = new Color(B.getRGB(i,j));
					r = (int)(a * rgb1.getRed() + be * rgb2.getRed());
					g = (int)(a * rgb1.getBlue() + be * rgb2.getBlue());
					b = (int)(a * rgb1.getGreen() + be * rgb2.getGreen());
				auxColor = new Color(r,g,b);
				res.setRGB(i - initW, j - initH, auxColor.getRGB());
			}
		}
		
	return res;
	}
	
	public static BufferedImage acopla(BufferedImage[] elemento, int filas, int columnas)
	{
		int n = elemento.length;
		int auxWidth = elemento[0].getWidth() * filas;
		int auxHeight = elemento[0].getHeight() * columnas;
		int inicioW = 0;
		int inicioH = 0;
		int tamW = elemento[0].getWidth();
		int tamH = elemento[0].getHeight();
		BufferedImage res = new BufferedImage(auxWidth,auxHeight,BufferedImage.TYPE_INT_RGB);
		
		Graphics g = res.getGraphics();
		for(int i = 0; i < n; i++)
		{
			g.drawImage(elemento[i], inicioW, inicioH, null);
			if((i+1) % filas != 0){
				inicioW = inicioW + tamW;
			}else{
				inicioW = 0;
				inicioH = inicioH + tamH;
			}
		}
		try{	
			ImageIO.write(res, "jpg", new File("C:\\Users\\ChristianPasten\\Desktop\\salida.jpg"));	
		}catch (IOException e) {
			e.printStackTrace();
		}

	return res;
	}
	
}

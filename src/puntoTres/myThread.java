package puntoTres;

import java.awt.image.BufferedImage;
import java.util.Random;

import puntoTres.Lock;
import puntoTres.Util;

public class myThread extends Thread {
	int myId;
	Lock lock;
	Random r = new Random();
	
	//****
		String simbolo;
		BufferedImage imgA, imgB;
		int filas, columnas;
		int alfa, beta;
		int inicio, fin;
	//***
	
	public myThread( int id, Lock lock)
	{
		myId = id;
		this.lock = lock;
	}
	
	public void nonCriticalRegion(){
		System.out.println( myId + " no est� en la CR");
		Util.mySleep( r.nextInt( 1000 ) );
	}
	
	public void CriticalRegion(){
		System.out.println( myId + " est� en la CR . . ." );
		//Aqui deberian ir las operaciones
		
		Util.mySleep( r.nextInt(1000));
	}
	
	public void run(){
		while( true ){
			lock.requestCR( myId );
			CriticalRegion();
			System.out.println("Libreando" + myId);
			lock.releaseCR( myId );
			nonCriticalRegion();
		}
	}
	
	public static BufferedImage parallelOperate (String simb,BufferedImage imgA, BufferedImage imgB, 
	int nFilas, int nCol,int alfa, int beta, int nThreads)
	{
		int nBloques = nFilas * nCol;
		int auxBlock = nBloques / nThreads;
		int inicio = 0;
		int fin = auxBlock;
		myThread hilos[] = new myThread[nThreads];
		Lock lock = new Bakery(nThreads);
		for(int i = 0; i < nThreads; i++)
		{
			myThread hilo = new myThread(i, lock, simb, alfa, beta, inicio, fin, imgA, imgB);
			hilos[i] = hilo;
			inicio += auxBlock;
			fin += auxBlock;
		}
		try{
			for(int i = 0; i < nThreads; i++)
			{
				hilos[i].start();
			}
		}catch(Exception e){e.printStackTrace();}
		
		
	}
}

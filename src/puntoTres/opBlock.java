package puntoTres;

import java.awt.image.BufferedImage;

public class opBlock {

	int nFilas, nCol, nBloques = 0;
	BufferedImage imgA, imgB;
	BufferedImage[] imgRes;
	
	public opBlock(int filas, int col, BufferedImage A, BufferedImage B){
		this.nFilas = filas;
		this.nCol = col;
		this.imgA = A;
		this.imgB = B;
	}
	
	public void calcBloques(){
		nBloques = nFilas * nCol;
	}
	
	public int getBloques(){
		return nBloques;
	}
	
	public void blockOperate(String simb, int alfa, int beta, int inicio, int fin)
	{
		int tamW = imgA.getWidth() / nFilas;
		int inicioW  = 0;
		int finW = tamW;
		int tamH = imgA.getHeight() / nCol;
		int inicioH = 0;
		int finH = tamH;
		calcBloques();
		
		//BufferedImage[] res = new BufferedImage[nBloques];
		Bloque[] partes = new Bloque[nBloques];
		for(int i = 0; i < nBloques; i++)
		{			
			Bloque parte = new Bloque(simb, inicioW, finW, inicioH, finH, imgA, imgB);
			if(simb == "\u03B1 - \u03B2"){
				parte.setAlfa(alfa);
				parte.setBeta(beta);
			}
			partes[i] = parte;
			if((i+1) % nFilas != 0){
				inicioW = inicioW + tamW;
				finW = finW + tamW;
			}else{
				inicioW = 0;
				finW = tamW;
				inicioH = inicioH + tamH;
				finH = finH + tamH;
			}
		}
		
		try {
			for (int i = inicio; i < fin; i++)
			{
				partes[i].ejecuta();
				//res[i] = partes[i].getResult();
				imgRes[i] = partes[i].getResult(); 
			}
			//this.imgRes = (ImageOperator.acopla(res, nFilas, nCol));
		} catch (Exception e) {};

	return;
	}
	
	public BufferedImage[] getResults(){ 	return this.imgRes;		}
	
}
